import argparse

from pathlib import Path

from core import connect_to_switch, sftp_read_file
from pretty import beautify_conf

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script de backup de la configuration des switchs")
    parser.add_argument("switch_name", type=str, help="Sauvegarde la configuration de ce switch")
    parser.add_argument("-n", "--no-pretty", action="store_true", help="N'affiche pas la version pretty")
    parser.add_argument("-c", "--case-size",type=int, help="Taille de la case", default=10)
    args = parser.parse_args()



    print("Backing up {}".format(args.switch_name))
    session = connect_to_switch("{}.switches.crans.org".format(args.switch_name), key="/root/.ssh/id_rsa")
    config = sftp_read_file(session, "cfg/running-config")
    if not args.no_pretty:
        beautify_conf(config.decode("utf-8").split("\n"), case_size=args.case_size)
    Path("configs-backup").mkdir(exist_ok=True)
    with open("configs-backup/{}.bak".format(args.switch_name), "wb") as config_bak:
        config_bak.write(config)
