def pprint_interfaces(i_dict, case_size=10):
    for module, inter in i_dict.items():
        lines = ["", "", "", ""]
        if module != "":
           print("Module " + module)
        for k, v in inter.items():
            lines[0] += "-" * case_size + "|"
            lines[1] += str(k) + " " * (case_size - len(str(k))) + "|"
            name = v.get("name", "")
            if len(name) > case_size:
                name = name[:case_size]
            lines[2] += name + " " * (case_size - len(name)) + "|"
            lines[3] += "-" * case_size + "|"
        last_i = 0
        step = 100//(case_size + 1) * (case_size + 1)
        first_row = True
        if len(lines[0]) <= step:
            for line in lines:
                print("|" + line)
        else:
            for i in range(step, len(lines[0]), step):
                for line in lines:
                    if first_row:
                        print("   |" + line[last_i:i] + "...")
                    else:
                        print("..." + line[last_i-1:i] + "...")
                first_row = False
                last_i = i
                print()
            for line in lines:
                if line != "":
                    print("..." + line[last_i-1:])


def beautify_conf(config_file, case_size=10):
    vlan_dict = dict()
    interfaces_dict = dict()
    switch_infos = dict()
    current_interface = None
    current_module = None
    for line in config_file:
        if line.startswith("hostname"):
            switch_infos["hostname"] = line.split(" ")[1].strip("\"")
        if line.startswith("interface"):
            try:
               current_module = ""
               current_interface = int(line.split(" ")[1])
            except ValueError:
                current_module = line.split(" ")[1][0]
                current_interface = int(line.split(" ")[1][1:])
            if interfaces_dict.get(current_module) is None:
                interfaces_dict[current_module] = dict()
            if interfaces_dict[current_module].get(current_interface) is None:
                interfaces_dict[current_module][current_interface] = dict()
        if line.strip().startswith("name") and current_interface is not None:
            interfaces_dict[current_module][current_interface]["name"] = " ".join(line.strip().split(" ")[1:]).strip("\"")
        if line.strip().startswith("exit"):
            if current_interface is not None:
                current_interface = None
            if current_module is not None:
                current_module = None

    pprint_interfaces(interfaces_dict, case_size)

if __name__ == "__main__":
    beautify_conf(open("configs-backup/bata-0.bak", "r"))
