def syntax_from_range(value, vlan_syntax=False):
    if value == []:
        return None
    value = list(set(value))
    value.sort()
    prec = None
    syntax = ""
    in_range = False
    for i in value:
        if prec is None or prec != i - 1:
            if syntax == "":
                syntax = str(i)
            elif in_range:
                if vlan_syntax:
                    syntax += "-{} {}".format(prec,i)
                else:
                    syntax += "-{},{}".format(prec,i)
                in_range = False
            else:
                if vlan_syntax:
                    syntax += " {}".format(i)
                else:
                    syntax += ",{}".format(i)
                in_range = False
        elif i == max(value):
            syntax += "-{}".format(i)
        else:
            in_range = True
        prec = i
    if vlan_syntax:
        syntax += " "
    return syntax

def range_from_syntax(value, vlan_syntax=False):
    if value is None:
        return []
    ret_range = list()
    if vlan_syntax:
        value = str(value).split(" ")
    else:
        value = str(value).split(",")
    for v in value:
        v = v.split("-")
        if len(v) == 2:
            v = list(range(int(v[0]), int(v[1])+1))
        elif len(v) == 1:
            v = [int(v[0])]
        else:
            raise RuntimeError("You fucked")
        ret_range += v
    return ret_range
