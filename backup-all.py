from pathlib import Path

from core import connect_to_switch, sftp_read_file

list_switches = [
    "bata-0",
    "bata-1",
    "bata-2",
    "bata-3",
    "bata-4",
    
    "backbone",
    "batb-0",
    "batb-1",
    "batb-2",
    "batb-3",
    "batb-4",
    "batb-5",
    # "batb-6", ça doit être le switch de la baie
    # "batb-7", c'était le switch de la med
   
    "batc-0",
    "batc-1",
    "batc-2",
    "batc-3",
    "batc-4",

    "batg-0",
    "batg-1",
    "batg-2",
    "batg-3",
    "batg-4",
    "batg-5",
    "batg-6",
    # "batg-7", # il existe pas
    "batg-8",
    "batg-9",
    
    "bath-0",
    "bath-1",
    "bath-2",
    "bath-3",

    "bati-0",
    "bati-1",
    # "bati-2", # il existe pas
    "bati-3",

    "batj-0",
    "batj-1",
    "batj-2",
    "batj-3", 
    "batj-4",

    # "batk-0", RIP la Kfet 
    
    "batm-0",
    "batm-1",
    "batm-2",
    "batm-3",
    "batm-4",
    "batm-5",
    "batm-6",
    "batm-7",
    # "batm-8", il a disparu

    "manoir-0",

]


if __name__ == "__main__":
    for switch in list_switches:
        print("Backing up {}".format(switch))
        session = connect_to_switch("{}.switches.crans.org".format(switch), key="/root/.ssh/id_rsa")
        config = sftp_read_file(session, "cfg/running-config")
        Path("configs-backup").mkdir(exist_ok=True)
        with open("configs-backup/{}.bak".format(switch), "wb") as config_bak:
            config_bak.write(config)
        
