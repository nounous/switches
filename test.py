import socket

from ssh2.session import Session
from ssh2.utils import wait_socket

class Test()

def get_output(channel):
    out = b""
    size, data = channel.read(255)
    while True:
        out += data
        if size < 255:
            break
        size, data = channel.read(255)
    return out.decode()


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.1.3", 22))
s.setblocking(False)
session = Session()
session.handshake(s)
session.userauth_password("crans", "test1234")
chan = session.open_session()
chan.shell()
get_output(chan)
get_output(chan)
chan.write("\n")
get_output(chan)  # random shit
chan.write("show run\n")
get_output(chan)  # du garbage après l'exécution d'une commande
running_config = get_output(chan)
print(running_config)

chan.close()
